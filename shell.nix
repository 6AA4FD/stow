{ pkgs ? import <nixpkgs> {}, local ? import ./default.nix}:
with pkgs;
stdenv.mkDerivation {
  name = "myhome";
  buildInputs = [
    stow
    gnumake
  ];
}
