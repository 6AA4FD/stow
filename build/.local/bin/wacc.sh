#!/bin/sh
xsetwacom set "Wacom Intuos S Pad pad" Button 1 "key Next"
xsetwacom set "Wacom Intuos S Pad pad" Button 2 "key Prior"
xsetwacom set "Wacom Intuos S Pad pad" Button 3 "key F1"
xsetwacom set "Wacom Intuos S Pad pad" Button 8 "key F2"
