#!/bin/sh
export GOPATH=$HOME/.go

PATH=/sbin:$PATH
PATH=~/.local/bin:$PATH
PATH=$GOPATH/bin:$PATH
PATH=$PATH:/usr/local/texlive/2018/bin/amd64-freebsd
PATH=$PATH:/home/quinn/.nimble/bin
export PATH

export TERMINAL="st"

export TEXMFHOME=~/.texmf

export STOW_PATH=$HOME/stow

NIX_PATH=$NIX_PATH:nixpkgs-overlays=$HOME/.nixpkgs/overlays
