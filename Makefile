PACKAGES = build static
.PHONY: install clean sudoinstall

install:
	stow --target=/home/quinn $(PACKAGES)

nixinstall:
	mkdir -p /etc/nixos/git-controlled
	cp -r Nix/* /etc/nixos/git-controlled

clean:
	stow -D $(PACKAGES) schemed
	rm -rfv schemed
