#!/bin/sh
# import the lesskey file from ~/.config/lesskey
LESSKEYFILE="$HOME/.config/lesskey"
if [ -f $LESSKEYFILE ]; then
    lesskey $LESSKEYFILE
else
    echo "you don't have a lesskey file, this should not be the case"
fi

