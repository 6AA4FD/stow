{ pkgs, ... }:
let

	evalNixos = configuration: import <nixpkgs/nixos> { inherit configuration; };

	ephemeral = import ./Nix/machines/ephemeral {} ;

in {

	pix.ipxe = ephemeral.pix.ipxe;

	studio-pc.iso = ephemeral.studio-pc.iso;

#	laptop.test = (evalNixos (import ./Nix/machines/quinn-laptop-lenovo)).config.system.build.initialRamdisk;

}
