{

	inputs = {
		nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-21.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    home-manager.url = "github:nix-community/home-manager/release-21.11";
		secrets.url = "/home/quinn/Misc/Important/Secrets";
		mmenu.url = "gitlab:6AA4FD/mmenu";
		rycee-nur.url = gitlab:rycee/nur-expressions;
		rycee-nur.flake = false;
    quinn-nixpkgs.url = gitlab:6AA4FD/nix-packages;
	};

		outputs = { self, ... } @ inputs: {

			nixosConfigurations = {

        fwun = inputs.nixpkgs-stable.lib.nixosSystem rec {
          system = "x86_64-linux";
          modules =  [
            "${inputs.nixpkgs-stable}/nixos/modules/installer/cd-dvd/installation-cd-graphical-plasma5.nix"
            { programs.adb.enable = true; }
          ];
        };

				quinn-workstation = inputs.nixpkgs-stable.lib.nixosSystem rec {
					system = "x86_64-linux";
					modules = [
						./Nix/machines/quinn-workstation
						inputs.home-manager.nixosModules.home-manager
						inputs.secrets.nixosModule
						{ nixpkgs.overlays = with inputs; [ mmenu.overlay ]; }
					];
					specialArgs = {
            inherit inputs;
            pkgs-unstable = import inputs.nixpkgs-unstable {
             system = system;
             config = { allowUnfree = true; };
            };
          };
				};

				quinn-workstation-2 = inputs.nixpkgs-stable.lib.nixosSystem rec {
					system = "x86_64-linux";
					modules = [
						./Nix/machines/quinn-workstation-2
						inputs.home-manager.nixosModules.home-manager
						inputs.secrets.nixosModule
						{ nixpkgs.overlays = with inputs; [ mmenu.overlay quinn-nixpkgs.overlay ]; }
					];
					specialArgs = {
            inherit inputs;
            pkgs-unstable = import inputs.nixpkgs-unstable {
             system = system;
             config = { allowUnfree = true; };
            };
          };
				};

				rock64-pbx = inputs.nixpkgs-stable.lib.nixosSystem rec {
					system = "aarch64-linux";
					modules = [
						./Nix/machines/rock64-pbx
						inputs.home-manager.nixosModules.home-manager
						inputs.secrets.nixosModule
					];
					specialArgs = { inherit inputs; };
				};

			};

			nixosModules = {

				audio = import ./Nix/modules/audio;

			};

  };

}
