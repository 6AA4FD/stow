{ config, lib, pkgs, ... }:

let
#	secrets = import ../../../secrets;
in
{

	imports = (import ../../modules) ++ [
	];

	home.home-manager.users.quinn = { pkgs, ...}: {
		services.mpd = {
			enable = true;
			musicDirectory = "${config.users.users.quinn.home}/Music/Sources";
		};
	};

	nix = {
		distributedBuilds = true;
		buildMachines = [
			{
				hostName = "10.10.10.25";
				system = "x86_64-linux";
				maxJobs = 2;
				speedFactor = 2;
				supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
			}
		];
	};

	networking.hostName = "quinn-laptop-lenovo";

	boot.loader.systemd-boot.enable = true;
	boot.loader.efi.canTouchEfiVariables = true;
	boot.supportedFilesystems = [ "zfs" ];
	boot.loader.systemd-boot = { consoleMode = "max"; };
	boot.plymouth.enable = true;
	networking.hostId = "0f7650ce";
	services.zfs = { trim.enable = true; autoScrub.enable = true;
		autoSnapshot = {
			enable = true;
		};
	};
	boot.kernelParams = [ "elevator=none" ];

	networking.interfaces.enp1s0.useDHCP = true;
	networking.interfaces.wlp2s0b1.useDHCP = true;

	time.timeZone = "America/Los_Angeles";

	services.logind.lidSwitch = "ignore";

	system.stateVersion = "20.03"; # do not touch

	nixpkgs = {
		config.allowUnfree = true;
		overlays = (import ../../overlays) ++ [];
	};

	my.emulation.udevRules.enable = true;
	my.emulation.packages.enable = true;

	my.build.enable.quinn-workstation = true;

#	my.pbx = { enable = true; cdr_db.enable = true; };

	my.audio = {
		limits.enable = true;
		overlay.enable = true;
		pluginPaths.enable = true;
		productionPackages.enable = true;
		kernel.patches.preempt_rt.enable = false;
		jack.enable = true;
	};

	my.graphical.desktop.enable = true;
	my.graphical.desktop.outputs = { "Unknown ASUS VA27EHE L3LMTF203058" = { mode = "1920x1080@75Hz"; }; };

	i18n = {
		inputMethod = {
			enabled = "ibus";
			ibus.engines = with pkgs.ibus-engines; [ mozc ];
		};
	};

	services = {
		syncthing = rec {
			enable = true;
			dataDir = "${config.users.users.quinn.home}";
			openDefaultPorts = true;
			user = "quinn";
			declarative = with (import ../../modules/syncthing); {
				devices = {
					nix-server = mySt.devices.nix-server;
					quinn-op6t = mySt.devices.quinn-op6t;
					quinn-workstation = mySt.devices.quinn-workstation;
				};
				overrideFolders = true;
				folders = {
					Misc = mySt.folders.Misc // {
						devices = [ "quinn-workstation" "quinn-op6t" ];
						path = dataDir + "/Misc";
					};
					MusicPhone = mySt.folders.MusicPhone //  {
						devices = [ "quinn-workstation" "quinn-op6t" ];
						path = dataDir + "/Music/Phone";
					};
					MusicSources = mySt.folders.MusicSources // {
						devices = [ "quinn-workstation" ];
						path = dataDir + "/Music/Sources";
					};
				};
			};
		};
	};

	environment.systemPackages = with pkgs; [
		# Internet
		myWeechat openvpn nicotine-plus
		# Visual Art
		imagemagick gimp krita gmic_krita_qt
		# Writing
		mupdf okular evince zotero ispell libreoffice-still
		# Music Listening
		ncmpcpp  mpc_cli beets
		# Uncategorized
		virt-manager
		steam steam-run
		keepassxc
		fzf
		anki
		inotifyTools
		direnv
		any-nix-shell
		pet
#		zoom-us
		hlint
		cabal2nix
		mmenu
	];

	fonts.fonts = with pkgs; [
		eb-garamond
		ibm-plex
		iosevka-term
		iosevka-term-lig
		iosevka-type
		ipaexfont
	];

}
