{ config, lib, pkgs, home-manager, inputs, ... }:

let

  pkgs-unstable = import inputs.nixpkgs-unstable {
    system = config.nixpkgs.system;
    config = { allowUnfree = true; };
  };

in rec {

	imports = (import ../../modules) ++ [
		./hardware-configuration.nix
	];

  boot.extraModulePackages = [ config.boot.kernelPackages.sysdig ];
	boot.kernelPackages = pkgs.linuxPackages;
	boot.kernelModules = [ "kvm-intel" "kvm-amd" ];
	virtualisation.libvirtd.enable = true;
	virtualisation.docker.enable = true;
	networking.dhcpcd.denyInterfaces = [ "macvtap0@*" ];
	networking.interfaces = {
		enp3s0f0.useDHCP = false;
		enp3s0f1.useDHCP = false;
		enp4s0f0.useDHCP = false;
		enp4s0f1.useDHCP = false;
	};

  services.minecraft-server = {
    enable = true;
    eula = true;
    package = pkgs.papermc;
    openFirewall = true;
    declarative = true;
    serverProperties = {
      server-port = 60690;
      difficulty = "normal";
      force-gamemode = true;
      gamemode = "survival";
      max-players = 25;
      pvp = true;
      motd = "bazinga";
      white-list = true;
      enable-rcon = false;
      "rcon.password" = "hunter2";
    };
    jvmOpts = "-Xmx6G -Xmx16G";
    whitelist = {
      xbadxgood = "9458083a-6e76-45dd-a990-5c1c04e46f9a";
      QUINNQUINNQUINN = "27f3ab3b-81f6-4b8a-9446-06eb3acd3c39";
    };
  };

	home-manager.users.quinn = { pkgs, ...}: {
		services.mpd = {
			enable = true;
			musicDirectory = "${config.users.users.quinn.home}/Music/Sources";
			network.listenAddress = "any";
		};
		services.mpdris2 = {
			enable = true;
			mpd.host = "127.0.0.1";
			multimediaKeys = true;
			notifications = true;
		};
		home.stateVersion = "20.09";
	};

	networking.hostName = "quinn-workstation";

	powerManagement.cpuFreqGovernor = "performance";

	boot.loader.systemd-boot.enable = true;
	boot.loader.efi.canTouchEfiVariables = true;
	boot.loader.systemd-boot = { consoleMode = "max"; };
	boot.loader.grub.enable = true;
	boot.loader.grub.version = 2;
	boot.loader.grub.device = "/dev/sda";
	boot.plymouth.enable = true;
	networking.hostId = "0f7650ce";

	time.timeZone = "America/Los_Angeles";

	services.logind.lidSwitch = "ignore";

	system.stateVersion = "20.09"; # on release, check notes and then increment

	nixpkgs = {
		config.allowUnfree = true;
		overlays = (import ../../overlays) ++ [ ];
	};

	my.audio = {
		limits.enable = true;
		overlay.enable = true;
		pluginPaths.enable = true;
		productionPackages.enable = true;
		kernel.patches.preempt_rt.enable = false;
		jack.enable = true;
    a2jmidi.enable = false;
		services.new-session-manager.enable = true;
	};

	my.graphical.desktop.enable = true;
  my.graphical.desktop.latitude = "38.444660";
  my.graphical.desktop.longitude = "-122.720306";
	my.graphical.desktop.outputs = { "Unknown ASUS VA27EHE L3LMTF203058" = { mode = "1920x1080@75Hz"; }; };

	my.monitoring = {
		dashboard.enable = true;
		node.enable = true;
		prometheus.enable = true;
    influxdb.enable = true;
	};

	i18n = {
		inputMethod = {
			enabled = "ibus";
			ibus.engines = with pkgs.ibus-engines; [ mozc ];
		};
	};

	services = {
		syncthing = rec {
			package = pkgs.syncthing;
			enable = true;
			dataDir = "${config.users.users.quinn.home}";
			openDefaultPorts = true;
			user = "quinn";
			declarative = with (import ../../modules/syncthing); {
				devices = {
					quinn-op6t = mySt.devices.quinn-op6t;
					quinn-workstation-2 = mySt.devices.quinn-workstation-2;
				};
				overrideFolders = true;
				folders = {
					Misc = mySt.folders.Misc // {
						devices = [ "quinn-op6t" "quinn-workstation-2" ];
						path = dataDir + "/Misc";
					};
					MusicPhone = mySt.folders.MusicPhone //  {
						devices = [ "quinn-op6t" "quinn-workstation-2" ];
						path = dataDir + "/Music/Phone";
					};
					MusicSources = mySt.folders.MusicSources // {
						devices = [ "quinn-workstation-2" ];
						path = dataDir + "/Music/Sources";
					};
				};
			};
		};
	};

	services.lorri.enable = true;

	environment.systemPackages = with pkgs; [
		# Internet
		myWeechat nicotine-plus
		# Visual Art
		imagemagick gimp krita gmic_krita_qt
		# Writing
		mupdf okular evince zotero ispell libreoffice-still
		# Music Listening
		ncmpcpp  mpc_cli beets
		# Uncategorized
		librecad
		virt-manager
		keepassxc
		fzf
		# anki
		inotifyTools
		direnv
		any-nix-shell
		pet
		hlint
		cabal2nix
		mmenu
		minikube
		kubectl
    kustomize
    kubernetes-helm
		dynamips
		vpcs
		multimc
		gucharmap
		# gImageReader
		(callPackage ../../packages/3tk {})
		# dolphinEmuMaster
    steam
    appimage-run
		# osu-lazer
    # slippi-netplay
    # slippi-playback
	];

  services.flatpak.enable = true;
  xdg.portal.enable = true;

	fonts.fonts = with pkgs; [
		eb-garamond
		ibm-plex
#		iosevka-term
#		iosevka-term-lig
#		iosevka-type
		ipaexfont
	];

	networking.firewall.allowedTCPPorts = [ 27036 27037 ];
	networking.firewall.allowedUDPPorts = [ 27031 27036 ];

	services.xserver.videoDrivers = [ "amdgpu" ];

	services.znc = {
		enable = true;
		mutable = false;
		useLegacyConfig = false;
		config = {
			Listener.l = {
				Port = 6969;
				Ipv4 = true;
				Ipv6 = false;
				SSL = true;
			};
			User.quinn = {
				Admin = true;
				AltNick = "quinn_";
				Ident = "quinn";
				LoadModule = [
					"chansaver"
				];
				Network.freenode = {
					Server = "chat.freenode.net +6697 " + config.my.secrets.freenodePass;
					Chan = { "#jack" = {}; "#context" = {}; "#nixos" = {}; "#neovim" = {}; "#haskell" = {}; "#home-manager" = {}; "#ardour" = {}; "#nixos-aarch64" = {}; };
					LoadModule = [ "nickserv" ];
					JoinDelay = 2;
					IRCConnectEnabled = true;
				};
				Pass.password = config.my.secrets.zncPass;
				Nick = "quinn";
				RealName = "Quinn";
			};
		};
	};

}
