{ system ? "x86_64-linux", nixpkgs ? import <nixpkgs> { inherit system; }, nixos ? import <nixpkgs/nixos> { inherit system; } }:

let

	androidflash = {
		imports = [ <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-graphical-plasma5-new-kernel.nix> ];
		networking.hostName = "androidflash";
		networking.useDHCP = true;
    programs.adb.enable = true;
		environment.systemPackages = with nixpkgs.pkgs; [ firefox ];
	};

	studioconfig = {
		imports = [ <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-graphical-plasma5-new-kernel.nix> ];
		networking.hostName = "studio-pc";
		networking.useDHCP = true;
		environment.systemPackages = with nixpkgs.pkgs; [ libreoffice-still gimp darktable ];
	};

	pixconfig = {
		imports = (import ../../modules) ++ [ <nixpkgs/nixos/modules/installer/netboot/netboot-minimal.nix> <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix> ];
		networking.networkmanager.enable = false;
		networking.wireless.enable = false;
		networking.useDHCP = true;
		systemd.services.sshd.wantedBy = nixpkgs.lib.mkOverride 40 [ "multi-user.target" ];
		networking.hostName = "pix-nixos";
	};

	evalNixos = configuration: import <nixpkgs/nixos> { inherit system configuration; };

	mkPlasmaIso = configuration: (evalNixos configuration).config.system.build.isoImage;

	mkNetboot = configuration: nixpkgs.pkgs.symlinkJoin {
		name = "netboot";
		paths = with (evalNixos configuration).config.system.build; [
			netbootRamdisk
			kernel
			netbootIpxeScript
		];
		preferLocalBuild=true;
	};

in {
	studio-pc.iso = mkPlasmaIso studioconfig;
  androidflash.iso = mkPlasmaIso androidflash;
	pix.ipxe = mkNetboot pixconfig;
}
