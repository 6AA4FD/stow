{ config, lib, pkgs, ... }:

{

	imports = [ ../../../modules/pbx ];

	networking.hostName = "testvm-aarch64";

	boot.loader.grub.enable = false;
	boot.loader.generic-extlinux-compatible.enable = true;
	boot.kernelPackages = pkgs.linuxPackages_latest;

	services.asterisk.enable = true;

	users.users.test = { isNormalUser = true; initialPassword = "doggy"; extraGroups = [ "wheel" ]; };

	fileSystems = {
		"/" = {
			device = "/dev/disk/by-label/NIXOS_SD";
			fsType = "ext4";
		};
	};

}
