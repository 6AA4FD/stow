{ config, pkgs, lib, ... }:

let

	zones = builtins.path { path = ./zones; };

in {

	imports = import ../../modules;

	boot.loader.grub.enable = false;

	boot.loader.generic-extlinux-compatible.enable = true;

#	boot.kernelPackages = pkgs.linuxPackages_latest;

	fileSystems = { "/" = { device = "/dev/disk/by-label/NIXOS_SD"; fsType = "ext4"; }; };

	networking.hostName = "rock64-pbx";

	system.stateVersion = "20.03"; # do not touch

	services.coredns = {
		enable = false;
		config = ''
.:53 {
	log
	errors
	loop
	health :5333
	ready
	prometheus 0.0.0.0:9153
	reload 30s
	loadbalance
	chaos
	forward . 8.8.8.8
	auto floor.ii. {
		directory ${zones}
	}
}
		'';
	};

	services.iperf3 = {
		enable = true;
		openFirewall = true;
		verbose= true;
	};

	my = {
		pbx.enable = true;
		pbx.cdr_db.enable = true;
		monitoring = {
			dashboard.enable = true;
			influxdb.enable = true;
			node.enable = true;
			winClients.enable = true;
			net.enable = true;
			prometheus.enable = true;
			coredns.enable = true;
		};
	};

	networking.firewall.allowedUDPPorts = [
		53 /* dns */
		3478 /* STUN for unifi */
	];

	networking.firewall.allowedTCPPorts = [
		8443 /* unifi web admin */
		9153 /* coredns prometheus endpoint */
	];

	services.unifi.enable = true;

	nixpkgs.config.allowUnfree = true;

}
