{ config, lib, pkgs, ... }:

{

	imports = (import ../../modules) ++ [
	];

	networking.hostName = "nix-server";

	time.timeZone = "America/Los_Angeles";

	system.stateVersion = "20.03"; # do not touch

	boot.loader.grub = {
		enable = true;
		version = 2;
		device = "/dev/sdc";
	};

	networking.useDHCP = false;
	networking.interfaces.ens3.useDHCP = true;

#	services.hydra = {
#		enable = true;
#		hydraURL = "http://localhost:3000";
#		notificationSender = "hydra@localhost";
#		buildMachinesFiles = [];
#	};

	users.extraUsers.sync = {
		home = "/mnt/sync";
		isNormalUser = true;
	};

	services.iperf3 = {
		enable = true;
		openFirewall = true;
		verbose= true;
	};

	services.syncthing = rec {
		enable = true;
		dataDir = "/mnt/sync";
		openDefaultPorts = true;
		user = "sync";
		guiAddress = "0.0.0.0:8384";
		declarative = with (import ../../modules/syncthing); {
			overrideDevices = true;
			devices = {
				quinn-laptop-lenovo = mySt.devices.quinn-laptop-lenovo;
				quinn-op6t = mySt.devices.quinn-op6t;
			};
			overrideFolders = true;
			folders = {
				"Misc" = mySt.folders.Misc // {
					devices = [ "quinn-laptop-lenovo" "quinn-op6t" ];
					path = dataDir + "/Misc";
				};
				"MusicPhone" = mySt.folders.MusicPhone // {
					devices = [ "quinn-laptop-lenovo" "quinn-op6t" ];
					path = dataDir + "/Music/Phone";
				};
				"MusicSources" = mySt.folders.MusicSources // {
					devices = [ "quinn-laptop-lenovo" ];
					path = dataDir + "/Music/Sources";
				};
			};
		};
	};

	my = {
		pbx.enable = true;
		pbx.cdr_db.enable = true;
		monitoring.dashboard.enable = true;
		monitoring.node.enable = true;
	};

	networking.firewall.allowedTCPPorts = [
		3000 # hydra gui
		8384 # syncthing gui
	];

}
