{ config, lib, pkgs, home-manager, inputs, ... }:

let

in rec {

	imports = (import ../../modules) ++ [
		./hardware-configuration.nix
	];

  boot.extraModulePackages = [ ];
	boot.kernelPackages = pkgs.linuxPackages-rt_latest;
	virtualisation.libvirtd.enable = true;
	virtualisation.docker.enable = true;
	networking.dhcpcd.denyInterfaces = [ "macvtap0@*" ];

	my.emulation.udevRules.enable = true;
	my.emulation.packages.enable = true;

	home-manager.users.quinn = { pkgs, ...}: {
    services.mopidy = {
      enable = true;
      extensionPackages = with pkgs; [ mopidy-iris mopidy-local ];
      settings = {
        local.media_dir = "/home/quinn/Music/Sources";
        file = {
          enabled = true;
          media_dirs = "/home/quinn/Music/Sources|Quinn-Collection";
        };
      };
    };
    programs.beets = {
      enable = true;
      settings = {
        plugins = "convert";
        directory = "/home/quinn/Music/Sources";
        library = "/home/quinn/Music/Sources/.library.db";
        import = { move = true; write = true; };
        convert = {
          auto = false;
          quiet = true;
          extension = "opus";
          format = "opus96";
          formats.opus96 = { command = "ffmpeg -i $source -y -acodec libopus -b:a 96k -vbr on -compression_level 10 $dest"; extension = "opus"; };
        };
      };
    };
		home.stateVersion = "22.11";
	};

	networking.hostName = "quinn-workstation-2";
  networking.hostId = "2e979725";

	boot.loader.systemd-boot.enable = true;
	boot.loader.efi.canTouchEfiVariables = true;
	boot.loader.systemd-boot = { consoleMode = "max"; };

  boot.kernelModules = [ "kvm-amd" ];

	time.timeZone = "America/Los_Angeles";

	system.stateVersion = "21.11"; # on release, check notes and then increment

	nixpkgs = {
		config.allowUnfree = true;
		overlays = (import ../../overlays) ++ [ ];
	};

	my.audio = {
		limits.enable = true;
		overlay.enable = false;
		pluginPaths.enable = true;
		productionPackages.enable = true;
		jack.enable = false;
    a2jmidid.enable = false;
		services.new-session-manager.enable = false;
	};

	my.graphical.desktop.enable = true;
  my.graphical.desktop.latitude = "38.444660";
  my.graphical.desktop.longitude = "-122.720306";
	my.graphical.desktop.outputs = { "Unknown ASUS VA27EHE L3LMTF203058" = { mode = "1920x1080@75Hz"; }; };

	i18n = {
		inputMethod = {
			enabled = "ibus";
			ibus.engines = with pkgs.ibus-engines; [ mozc ];
		};
	};

	services = {
    pipewire = {
      enable = true;
      jack.enable = true;
      alsa = { enable = true; support32Bit = true; };
      pulse.enable = true;
      config.pipewire = {
       "context.properties" = {
         "link.max-buffers" = 16;
         "log.level" = 2;
         "default.clock.rate" = 48000;
         "default.clock.quantum" = 1024;
         "default.clock.min-quantum" = 32;
         "default.clock.max-quantum" = 2000;
         "core.daemon" = true;
         "core.name" = "pipewire-0";
       };
       "context.modules" = [
        {
          name = "libpipewire-module-rtkit";
          args = {
            "nice.level" = -15;
            "rt.prio" = 88;
            "rt.time.soft" = 200000;
            "rt.time.hard" = 200000;
          };
        flags = [ "ifexists" "nofail" ];
        }
        { name = "libpipewire-module-protocol-native"; }
        { name = "libpipewire-module-profiler"; }
        { name = "libpipewire-module-metadata"; }
        { name = "libpipewire-module-spa-device-factory"; }
        { name = "libpipewire-module-spa-node-factory"; }
        { name = "libpipewire-module-client-node"; }
        { name = "libpipewire-module-client-device"; }
        {
        name = "libpipewire-module-portal";
        flags = [ "ifexists" "nofail" ];
        }
        {
        name = "libpipewire-module-access";
        args = {};
            }
          { name = "libpipewire-module-adapter"; }
          { name = "libpipewire-module-link-factory"; }
          { name = "libpipewire-module-session-manager"; }
        ];
      };
    };
    xrdp = {
      enable = true;
      openFirewall = true;
    };
		syncthing = with (import ../../modules/syncthing); rec {
			package = pkgs.syncthing;
			enable = true;
			dataDir = "${config.users.users.quinn.home}";
			openDefaultPorts = true;
			user = "quinn";
			devices = {
				quinn-op6t = mySt.devices.quinn-op6t;
				quinn-op6t2 = mySt.devices.quinn-op6t2;
				quinn-op6t3 = mySt.devices.quinn-op6t3;
         quinn-workstation = mySt.devices.quinn-workstation;
			};
			overrideFolders = true;
			folders = {
				Misc = mySt.folders.Misc // {
					devices = [ "quinn-op6t3" "quinn-workstation" ];
					path = dataDir + "/Misc";
				};
				MusicPhone = mySt.folders.MusicPhone //  {
					devices = [ "quinn-op6t3" "quinn-workstation" ];
					path = dataDir + "/Music/Phone";
				};
				MusicSources = mySt.folders.MusicSources // {
					devices = [ "quinn-workstation" ];
					path = dataDir + "/Music/Sources";
				};
			};
		};
	};

  services.lorri.enable = true;

	environment.systemPackages = with pkgs; [
    anki
		# Internet
		myWeechat nicotine-plus
		# Visual Art
		imagemagick gimp krita gmic_krita_qt
		# Writing
		mupdf okular evince zotero ispell libreoffice-still
		# Music Listening
		ncmpcpp  mpc_cli beets
		# Uncategorized
		librecad
		virt-manager
		keepassxc
		fzf
		inotifyTools
		direnv
		any-nix-shell
		mmenu
		dynamips
		vpcs
		gucharmap
		# gImageReader
		(callPackage ../../packages/3tk {})
    steam-run
    yuzu-ea
    (pkgs.kodi-wayland.withPackages (p: with p; [
      inputstream-adaptive
      inputstream-rtmp
      inputstream-ffmpegdirect
    ]))
	];

  services.flatpak.enable = true;
  xdg.portal.enable = true;
  xdg.portal.wlr.enable = true;

	fonts.fonts = with pkgs; [
		eb-garamond
		ibm-plex
		ipaexfont
	];

	networking.firewall.allowedTCPPorts = [ 27036 27037 4042 ];
	networking.firewall.allowedUDPPorts = [ 27031 27036 4042 ];

	services.xserver.videoDrivers = [ "amdgpu" ];

}
