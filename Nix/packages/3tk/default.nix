{ pkgs, ... }:

pkgs.python3.pkgs.buildPythonApplication rec {

	pname = "3tk";
	version = "0.2.0";

	buildInputs = [ pkgs.python3 ];

	src = pkgs.fetchFromGitLab {
		owner = "6AA4FD";
		repo = pname;
		rev = "v0.2";
		sha256 = "0z1b1d921wp4ydn0rzzjyh390mm2xzd1mbx6q6gvsk4k408pf3n9";
	};

}
