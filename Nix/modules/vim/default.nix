## TODO
# - vim that will lint/format (and interactively diff)
# - expandtab settings and such for haskell, python, etc
{pkgs, config, lib, home, ...}:

with lib;

let

	cfg = config.my.vim;

in {

	imports = [];

	options.my.vim = {
		enable = mkEnableOption "Turn on my vim config";
	};

	config = mkIf cfg.enable {

		home-manager.users.quinn = { pkgs, ...}: {
		nixpkgs.overlays = [ (import ../../overlays/personal.nix) ];
			programs.neovim = {
				enable = true; # missing vim-matchup
        plugins = with pkgs.vimPlugins; [
          pear-tree
          fzf-vim
          fzfWrapper
          incsearch-vim
          easymotion
          undotree
          nerdcommenter
          neoformat
        ];
				extraConfig = import ./init.vim.nix;
			};
#			home.packages = with pkgs; [ (mkIf config.my.graphical.desktop.enable unstableNeovimQt) ];
		};

		environment.systemPackages = with pkgs; [ vim ];

	};
}
