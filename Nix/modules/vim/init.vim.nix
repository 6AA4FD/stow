''
" Functional
let g:EasyMotion_smartcase = 1
set inccommand=nosplit
set smartcase
set backspace
set scrolloff=3
set backspace=indent,eol,start
" expandtab is preferred, but basically every open source project uses (2) spaces instead of tabs
set smarttab tabstop=2 shiftwidth=2 softtabstop=2 autoindent expandtab
set list listchars=tab:-\ ,trail:~
set spelllang=en_us
set clipboard+=unnamedplus
filetype plugin on

" Mappings
no J N
no j n
no n j
no N J
no e k
no E K
no k e
no K E
no i l
no I L
no l i
no L I

map ; :

map <leader>h :noh<CR>

map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

let g:EasyMotion_do_mapping = 0
map f <Plug>(easymotion-s)

map <leader>b :Buffers<CR>
map <leader>f :Files<CR>
map <leader>' :Marks<CR>
map <leader>r :Rg<CR>
map <leader>y :!nix build .\#
map <leader>t :set expandtab!<CR>

map <leader>u :UndotreeToggle<CR>

nnoremap <leader>wh <C-W><C-H> " Window movement
nnoremap <leader>wn <C-W><C-J>
nnoremap <leader>we <C-W><C-K>
nnoremap <leader>wi <C-W><C-L>

nmap <leader>n :%s/\s\+$//e<CR> " Clear trailing spaces
vmap <leader>n :s/\s\+$//e<CR>

map <leader>e :call CreateCenteredFloatingWindow()<CR>:Explore<CR>

"Colors
syntax off

let s:color_0 = '#2c1322'
let s:color_1 = '#422838'
let s:color_2 = '#664a5b'
let s:color_3 = '#806273'
let s:color_4 = '#a7889a'
let s:color_5 = '#c3a2b5'
let s:color_6 = '#dfbdd0'
let s:color_7 = '#ffe2f5'
let s:color_8 = '#b38b19'
let s:color_9 = '#ec4b24'
let s:color_10 = '#e52638'
let s:color_11 = '#db4ba8'
let s:color_12 = '#a855bc'
let s:color_13 = '#0091d1'
let s:color_14 = '#409cb3'
let s:color_15 = '#73ae22'

function! s:Hi(group, guifg, guibg)
	if a:guifg != ""
		exec "hi " . a:group . " guifg=" . a:guifg
	endif
	if a:guibg != ""
		exec "hi " . a:group . " guibg=" . a:guibg
	endif
endfunction

call s:Hi("LineNr", s:color_6, s:color_2)
call s:Hi("CursorLineNr", s:color_6, s:color_1)
call s:Hi("SignColumn", "", s:color_3)
call s:Hi("Normal", s:color_0, s:color_7)
call s:Hi("StatusLine", s:color_0, s:color_7)
call s:Hi("Whitespace", s:color_4, s:color_7)
call s:Hi("Floating", "", s:color_6)

"Visual
set number relativenumber

function! XHairs()
  set cursorline! cursorcolumn!
endfunction

augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
	autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

let g:lens#disabled_filetypes = [ 'fzf' ]

autocmd FileType fzf setlocal nonumber norelativenumber

function! CreateCenteredFloatingWindow()
    let width = min([&columns - 4, max([80, &columns - 20])])
    let height = min([&lines - 4, max([20, &lines - 10])])
    let top = ((&lines - height) / 2) - 1
    let left = (&columns - width) / 2
    let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}

    call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
    set winhl=Normal:Floating
endfunction

let g:fzf_layout = { 'window': 'call CreateCenteredFloatingWindow()' }

set splitbelow
set splitright

" GUI

function! OnUIEnter()
  set guifont=IBM\ Plex\ Mono:h12
endfunction

autocmd UIEnter * call OnUIEnter()

" Abbreviations

iab sha256dummy d3eb539a556352f3f47881d71fb0e5777b2f3e9a4251d283c18c67ce996774b7

''
