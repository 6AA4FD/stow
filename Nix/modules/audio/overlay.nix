self: super:

let

	unstable = (import <nixpkgs-unstable> {}).pkgs;

	rtConfig = with unstable.lib.kernel; { PREEMPT = yes; PREEMPT_RT_FULL = yes; HZ_1000 = yes; };

	rtPatch = { kbranch, kminor, kfix, sha256, pversion }: {
		name = "realtime";
		patch = super.fetchurl {
			inherit sha256;
			url = "mirror://kernel/linux/kernel/projects/rt/${kbranch}.${kminor}/patch-${kbranch}.${kminor}.${kfix}-${pversion}.patch.xz";
		};
	};

	rtKernel = { kbranch, kminor, kfix, pversion, kernelSha256, patchSha256 }: unstable.buildLinux rec {
		kversion = "${kbranch}.${kminor}.${kfix}";
		version = "${kversion}-${pversion}";
		src = super.fetchurl {
			url = "mirror://kernel/linux/kernel/v${kbranch}.x/linux-${kversion}.tar.xz";
			sha256 = kernelSha256;
		};
		structuredExtraConfig = rtConfig;
		kernelPatches = [ (rtPatch { inherit kbranch kminor kfix pversion; sha256 = patchSha256; }) ];
	};

in {

	rtLinux = rtKernel { # check out https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/ to find a compatible kernel and then put in (dummy) hashes to get it going
		kbranch = "5"; # then run linuxPackagesFor on it and recurseIntoAttrs on that.
		kminor = "6";
		kfix = "14";
		pversion = "rt7";
		kernelSha256 = "18vyxi64i93v4qyky5q62kkasm1da7wmz91xfkx3j7ki84skyxik";
		patchSha256 = "1yy7r88yr8b8m5mp0li0lwhynqzdgjkrb2qjqycz4hzpvrrn8mlm";
	};

	audioPackages = super.recurseIntoAttrs (super.linuxPackagesFor self.rtLinux);

	unstablePolyphone = unstable.polyphone;

}
