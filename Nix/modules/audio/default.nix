## TODO:
# - add rtkit
# - add patroneo
# - add agordejo
# - once available, add pipewire option
# - port musnix optimizations and patches
# - remove broken features
# - separate personal use packages into a separate module
# - icecast/AVTP (pref)/RTP sink service, hopefully through gstreamer
# - TSN/AVB support if possible
# - deprecate musnix
{system, inputs, config, lib, pkgs, ...}:

with lib;

let

	cfg = config.my.audio;

in {

	options.my.audio = {
		services.new-session-manager = {
			enable = mkEnableOption "turn on new-session-manager daemon";
			servicePackage = mkOption {
				type = types.package;
				default = pkgs.new-session-manager;
			};
		};
		limits = {
			enable = mkEnableOption "Use custom audio limits.";
			entries = mkOption {
				type = types.listOf types.attrs;
				default = [
					{ domain = "@audio"; item = "memlock"; type = "-"; value = "unlimited"; }
					{ domain = "@audio"; item = "rtprio"; type = "-"; value = "95"; }
					{ domain = "@audio"; item = "nofile"; type = "soft"; value = "99999"; }
					{ domain = "@audio"; item = "nofile"; type = "hard"; value = "99999"; }
				];};
		};
		overlay = {
			enable = mkEnableOption "Turn on audio overlay";
		};
		kernel.patches.preempt_rt.enable = mkEnableOption "Enable RT Kernel.";
		productionPackages = {
			enable = mkEnableOption "Load music production packages into environment.";
			pkgs = mkOption {
				type = types.listOf types.package;
				default = with pkgs; [
					ardour audacity calf hydrogen guitarix drumgizmo sorcer distrho helm yoshimi polyphone drumgizmo  lilypond musescore zyn-fusion vcv-rack midimonster carla puredata orca-c uxn puredata helvum bitwig-studio
				];
			};
		};
		pluginPaths = {
			enable = mkEnableOption "Turn on environment variables for finding plugins";
			entries = mkOption {
				default = {
					LV2_PATH = [ "/run/current-system/sw/lib/lv2" ];
					LADSPA_PATH = [ "/run/current-system/sw/lib/ladspa" ];
					LXVST_PATH = [ "/run/current-system/sw/lib/vst" "/run/current-system/sw/lib/vst3" ];
				};
			};
		};
    a2jmidid = {
      enable = mkEnableOption "Turn on a2jmidid D-Bus service";
			servicePackage = mkOption {
				type = types.package;
				default = pkgs.a2jmidid;
			};
    };
		jack = {
			enable = mkEnableOption "Turn on jackd";
			servicePackage = mkOption {
				type = types.package;
				default = pkgs.jack2Full;
			};
			packages = mkOption {
				type = types.listOf types.package;
				default = with pkgs; [ qjackctl cadence jack2Full jalv jack_rack lilv ];
				description = "Packages to add to environment for use with jack";
			};
		};
	};
	config = {
		security.pam.loginLimits = mkIf cfg.limits.enable cfg.limits.entries;
		nixpkgs.overlays = mkIf cfg.overlay.enable [ (import ./overlay.nix) ];
		environment.variables = mkIf cfg.pluginPaths.enable cfg.pluginPaths.entries;
		environment.systemPackages = mkMerge [
			(mkIf cfg.jack.enable cfg.jack.packages)
			(mkIf cfg.productionPackages.enable cfg.productionPackages.pkgs)
			(mkIf cfg.services.new-session-manager.enable [ cfg.services.new-session-manager.servicePackage ])
		];
		boot.kernelPackages = mkIf cfg.kernel.patches.preempt_rt.enable pkgs.audioPackages;
		powerManagement.cpuFreqGovernor = mkIf cfg.kernel.patches.preempt_rt.enable "performance";
		boot.kernel.sysctl = mkIf cfg.kernel.patches.preempt_rt.enable { "vm.swappiness" = 10; "dev.hpet.max-user-freq"=3072; };
		services.udev.extraRules = (mkIf cfg.kernel.patches.preempt_rt.enable ''
			KERNEL=="rtc0", GROUP="audio"
			KERNEL=="hpet", GROUP="audio"
		'');
		boot.kernelModules = mkIf cfg.kernel.patches.preempt_rt.enable [ "snd-seq" "snd-rawmidi" ];
		hardware.pulseaudio.extraConfig = mkIf cfg.jack.enable ''
unload-module module-jackdbus-detect
load-module module-jack-sink connect=true
load-module module-jack-source connect=false
		'';
		systemd.user.services.pulseaudio = {
			after = [ (mkIf cfg.jack.enable "jack.service") ];
		};
		systemd.user.services.new-session-manager = mkIf cfg.services.new-session-manager.enable {
			description = "session manager for audio applications";
			wantedBy = [ "graphical-session.target" ];
			after = ["jack.service"];
			serviceConfig = {
				ExecStart = "${cfg.services.new-session-manager.servicePackage}/bin/nsmd";
			};
		};
    systemd.user.services.a2jmidid = mkIf cfg.a2jmidid.enable {
      wantedBy = [ "jack.service" ];
      after = [ "jack.service" ];
      serviceConfig = {
        ExecStart = "${cfg.a2jmidid.servicePackage}/bin/a2j_control start";
				ExecStop = "${cfg.a2jmidid.servicePackage}/bin/a2j_control stop";
				ExecStopPost = "${pkgs.procps}/bin/pkill -9 jackdbus";
				SuccessExitStatus = "0";
				RemainAfterExit = true;
				Type = "dbus";
				BusName = "org.gna.home.a2jmidid";
      };
      restartIfChanged = true;
    };
		systemd.user.services.jack = mkIf cfg.jack.enable {
			wantedBy = [ "graphical-session.target" ];
			requiredBy = [ "pulseaudio.service" ];
			before = [ "pulseaudio.service" ];
			description = "JACK 2 via dbus";
			serviceConfig = {
				ExecStart = "${cfg.jack.servicePackage}/bin/jack_control start";
				ExecStop = "${cfg.jack.servicePackage}/bin/jack_control stop";
				ExecStopPost = "${pkgs.procps}/bin/pkill -9 jackdbus";
				SuccessExitStatus = "0";
				RemainAfterExit = true;
				Type = "dbus";
				BusName = "org.jackaudio.Controller";
			};
			restartIfChanged = true;
		};
		services.dbus.packages = [ (mkIf cfg.jack.enable cfg.jack.servicePackage) (mkIf cfg.a2jmidid.enable cfg.a2jmidid.servicePackage) ];
		hardware.pulseaudio.package = (mkIf cfg.jack.enable pkgs.pulseaudioFull);
	};
}
