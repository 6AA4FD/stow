{ config }:
{
  conf = ''

[from-handset]
exten => _*XX,1,NoOp()
	same => n,Set(user=ann)
	same => n,Goto(internal-mgmt,''${EXTEN},1)
exten => _011.,1,Gosub(trunkdial,s,1(''${EXTEN:3}))
exten => _1NXXXXXXXXX,1,Gosub(trunkdial,s,1(''${EXTEN}))
exten => _NXXXXXXXXX,1,Gosub(trunkdial,s,1(1''${EXTEN}))
exten => _XXXXXXX,1,Gosub(trunkdial,s,1(1650''${EXTEN}))

[internal-mgmt]
exten => *99,1,VoiceMailMain(ann@default,s)

[from-external]
exten => ${config.my.secrets.pbx.localDid},1,NoOp()
	same => n,Zapateller(answer|nocallerid)
	same => n,Dial(PJSIP/ann, 25)
	same => n,VoiceMail(ann@default, u)
	same => n,Hangup()

[trunkdial]
exten => s,1,Dial(PJSIP/''${ARG1}@flowroute_endpoint)
exten => s,n,Goto(s-''${DIALSTATUS},1)
exten => s-NOANSWER,1,Hangup()
exten => s-BUSY,1,Hangup()
exten => _s-.,1,NoOp

'';
}
