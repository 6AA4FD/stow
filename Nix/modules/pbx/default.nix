{config, lib, pkgs, ...}:

with lib;
let
	cfg = config.my.pbx;
in {
	imports = [];
	options.my.pbx = {
		enable = mkEnableOption "Turn on my asterisk server.";
		cdr_db.enable = mkEnableOption "Turn on ODBC integration";
	};
	config = mkMerge [
		(mkIf cfg.cdr_db.enable {
			environment.unixODBCDrivers = with pkgs.unixODBCDrivers; [ mariadb ];
			environment.etc."odbc.ini".text = ''
[asterisk-connector]
Driver = MariaDB
Database = asterisk
User = asterisk
Socket = /run/mysqld/mysqld.sock
			'';
		})
		{
		nixpkgs.overlays = mkIf cfg.enable [ (import ./overlay.nix) ];
		services.asterisk = mkIf cfg.enable {
			enable = true;
			package = pkgs.myAsterisk;
			confFiles = {
				"pjsip.conf" = ''
[global]
debug=yes
keep_alive_interval=10

[transport-nat]
type=transport
protocol=udp
bind=0.0.0.0
local_net=10.0.0.0/24
local_net=127.0.0.1/32

[ann]
transport=transport-nat
type=endpoint
context=from-handset
auth=ann
aors=ann
direct_media=no
disallow=all
allow=ulaw
callerid="<${config.my.secrets.pbx.localDid}>"
mailboxes=ann@default

[ann]
type=auth
auth_type=userpass
password=${config.my.secrets.pbx.handsetPw}
username=ann

[ann]
type=aor
max_contacts=1
remove_existing=yes

[flowroute_registration]
type=registration
transport=transport-nat
outbound_auth=flowroute_auth
server_uri=sip:us-west-or.sip.flowroute.com
client_uri=sip:${config.my.secrets.pbx.trunkUser}@us-west-or.sip.flowroute.com
contact_user=${config.my.secrets.pbx.trunkUser}
retry_interval=60
forbidden_retry_interval=600
expiration=3600
line=yes
endpoint=flowroute_endpoint

[flowroute_auth]
type=auth
auth_type=userpass
password=${config.my.secrets.pbx.trunkPw}
username=${config.my.secrets.pbx.trunkUser}

[flowroute_endpoint]
type=endpoint
transport=transport-nat
context=from-external
outbound_auth=flowroute_auth
direct_media=no
aors=flowroute_aor
disallow=all
allow=ulaw
rtp_keepalive=10
rtp_symmetric=yes
bind_rtp_to_media_address=yes

[flowroute_aor]
type=aor
contact=sip:us-west-or.sip.flowroute.com

[flowroute_identify]
type=identify
endpoint=flowroute_endpoint
match=147.75.60.160/28
match=34.210.91.112/28
match=147.75.65.192/28
match=34.226.36.32/28
        '';
				"extensions.conf" = ''
[from-handset]
exten => _*XX,1,NoOp()
	same => n,Set(user=ann)
	same => n,Goto(internal-mgmt,''${EXTEN},1)
exten => _011.,1,Gosub(trunkdial,s,1(''${EXTEN:3}))
exten => _1NXXXXXXXXX,1,Gosub(trunkdial,s,1(''${EXTEN}))
exten => _NXXXXXXXXX,1,Gosub(trunkdial,s,1(1''${EXTEN}))
exten => _XXXXXXX,1,Gosub(trunkdial,s,1(1650''${EXTEN}))

[internal-mgmt]
exten => *99,1,VoiceMailMain(ann@default,s)

[from-external]
exten => ${config.my.secrets.pbx.localDid},1,NoOp()
	same => n,Zapateller(answer|nocallerid)
	same => n,Dial(PJSIP/ann, 25)
	same => n,VoiceMail(ann@default, u)
	same => n,Hangup()

[trunkdial]
exten => s,1,Dial(PJSIP/''${ARG1}@flowroute_endpoint)
exten => s,n,Goto(s-''${DIALSTATUS},1)
exten => s-NOANSWER,1,Hangup()
exten => s-BUSY,1,Hangup()
exten => _s-.,1,NoOp
        '';
				"voicemail.conf" = readFile ./voicemail.conf;
				"logger.conf" = readFile ./logger.conf;
				"cdr_odbc.conf" = mkIf cfg.cdr_db.enable (readFile ./cdr_odbc.conf);
				"res_odbc.conf" = mkIf cfg.cdr_db.enable (readFile ./res_odbc.conf);
			};
		};
		services.mysql = mkIf cfg.cdr_db.enable {
			enable = true;
			package = pkgs.mariadb;
			initialDatabases = [ { name = "asterisk"; schema = ./cdrtable.sql; } ];
			ensureUsers = [
				{ name = "asterisk";
					ensurePermissions = { "asterisk.*" = "ALL PRIVILEGES"; };
				}
				(mkIf config.my.monitoring.dashboard.enable { name = "grafana";
					ensurePermissions = { "asterisk.*" = "SELECT"; };
				})
			];
		};
		networking.firewall = mkIf cfg.enable {
			allowedUDPPorts = [ 5060 ];
			allowedUDPPortRanges = [ { from = 10000; to = 20000; } ];
		};
	}];
}
