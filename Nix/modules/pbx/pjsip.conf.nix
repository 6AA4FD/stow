{ config }:
{
  conf = ''
[global]
debug=yes
keep_alive_interval=10

[transport-nat]
type=transport
protocol=udp
bind=0.0.0.0
local_net=10.0.0.0/24
local_net=127.0.0.1/32

[ann]
transport=transport-nat
type=endpoint
context=from-handset
auth=ann
aors=ann
direct_media=no
disallow=all
allow=ulaw
callerid="<${config.my.secrets.pbx.localDid}>"
mailboxes=ann@default

[ann]
type=auth
auth_type=userpass
password=${config.my.secrets.pbx.handsetPw}
username=ann

[ann]
type=aor
max_contacts=1
remove_existing=yes

[flowroute_registration]
type=registration
transport=transport-nat
outbound_auth=flowroute_auth
server_uri=sip:us-west-or.sip.flowroute.com
client_uri=sip:${config.my.secrets.pbx.trunkUser}@us-west-or.sip.flowroute.com
contact_user=${config.my.secrets.pbx.trunkUser}
retry_interval=60
forbidden_retry_interval=600
expiration=3600
line=yes
endpoint=flowroute_endpoint

[flowroute_auth]
type=auth
auth_type=userpass
password=${config.my.secrets.pbx.trunkPw}
username=${config.my.secrets.pbx.trunkUser}

[flowroute_endpoint]
type=endpoint
transport=transport-nat
context=from-external
outbound_auth=flowroute_auth
direct_media=no
aors=flowroute_aor
disallow=all
allow=ulaw
rtp_keepalive=10
rtp_symmetric=yes
bind_rtp_to_media_address=yes

[flowroute_aor]
type=aor
contact=sip:us-west-or.sip.flowroute.com

[flowroute_identify]
type=identify
endpoint=flowroute_endpoint
match=147.75.60.160/28
match=34.210.91.112/28
match=147.75.65.192/28
match=34.226.36.32/28

'';
}
