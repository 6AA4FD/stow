super: self:

{
	myAsterisk = super.asterisk.overrideAttrs (oldAttrs: rec {
		buildInputs = oldAttrs.buildInputs ++ [ super.postgresql super.unixODBC ];
	});
}
