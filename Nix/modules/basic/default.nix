{ config, lib, pkgs, ... }:

rec {

	time.timeZone = lib.mkDefault "America/Los_Angeles";

	home-manager.users.quinn = {pkgs, ...}: {
		programs.zsh = {
			enable = true;
		};
		programs.git = {
			enable = true;
			userName = "Quinn";
			userEmail = "ff6442954@gmail.com";
			ignores = [ "*.orig" "*.BACKUP.*" "*.BASE.*" "*.LOCAL.*" "*.REMOTE.*" "*_BACKUP_*.txt" "*_BASE_*.txt" "*_LOCAL_*.txt" "*_REMOTE_*.txt" "dist" "dist-*" "cabal-dev" "*.o" "*.hi" "*.chi" "*.chs.h" "*.dyn_o" "*.dyn_hi" ".hpc" ".hsenv" ".cabal-sandbox/" "cabal.sandbox.config" "*.prof" "*.aux" "*.hp" "*.eventlog" ".stack-work/" "cabal.project.local" "cabal.project.local~" ".HTF/" ".ghc.environment.*" "*~" ".fuse_hidden*" ".directory" ".Trash-*" ".nfs*[._]*.s[a-v][a-z]" "[._]*.sw[a-p]" "[._]s[a-rt-v][a-z]" "[._]ss[a-gi-z]" "[._]sw[a-p]" "Session.vim" "Sessionx.vim" ".netrwhist" "tags" "[._]*.un~" ".vim*.zwc" "*.zwc.old" "*zcompdump*" ".zcalc_history" "._zplugin" ".zplugin_lstupd" "zsdoc/data.directory_history.venv" "/tests/_output/*" "!/tests/_output/.gitkeep" ];
      extraConfig = { safe.directory = "/home/quinn/Src/stow"; };
		};
		programs.fzf = { enable= true; enableZshIntegration = true; defaultCommand = "fd"; fileWidgetCommand = "fd --type f"; };
		home.sessionVariables = {
			EDITOR = "nvim";
			PAGER = "less";
		};
	};

	systemd.tmpfiles.rules = with users.users; [
		"d ${quinn.home}/tmp 0700 ${quinn.name} ${quinn.group} 7d"
	];

	boot.runSize = lib.mkDefault "3g";

	my.vim.enable = true;

	programs.zsh = {
		enable = true;
		setOptions = [ "AUTO_CD" "AUTO_PUSHD" "PUSHD_IGNORE_DUPS" "APPEND_HISTORY" "HIST_NO_STORE" "HIST_REDUCE_BLANKS" "ALIASES" "RM_STAR_WAIT" "PROMPT_BANG" "PROMPT_PERCENT" "PROMPT_SUBST" "NO_HUP" ];
		interactiveShellInit = ''
			alias e=exa
			alias l=${environment.variables.PAGER}
			alias f=fzf
			alias p=pwd
			alias d=$EDITOR
			eval "$(direnv hook zsh)"
			any-nix-shell zsh --info-right|source /dev/stdin
			bindkey -v                        # vi mode
			bindkey "^?" backward-delete-char # some bug if you don't have this
			export KEYTIMEOUT=1               # faster

			bindkey -a 'n' down-line-or-history
			bindkey -a 'e' up-line-or-history
			bindkey -a 'i' vi-forward-char

			rationalise-dot() {
			  if [[ $LBUFFER = *.. ]]; then
			    LBUFFER+=/..
			  else
			    LBUFFER+=.
			  fi
			}
			zle -N rationalise-dot
			bindkey . rationalise-dot

			bindkey -a 'm' vi-repeat-search
			bindkey -a 'M' vi-rev-repeat-search
			bindkey -a 'l' vi-insert
			bindkey -a 'u' vi-undo-change
			bindkey -a 'N' vi-join

			bindkey -a 'k' vi-forward-word-end
		'';
	};

	nix.gc = {
		automatic = true;
		dates = "weekly" ;
		options = "--delete-older-than 30d";
	};

	programs = {
		ssh.startAgent = true;
		mosh.enable = true;
	};

	programs.tmux = {
		enable = true;
		newSession = true;
		terminal = "screen-256color";
		clock24 = true;
	};

	services = {
		openssh = {
			enable = true;
			passwordAuthentication = false;
		};
	};

	security.sudo.wheelNeedsPassword = false;

	security.polkit.extraConfig = ''
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});
	'';

	users.users.quinn = {
		description = "Quinn";
		isNormalUser = true;
		home = "/home/quinn";
		name = "quinn";
		group = "users";
		extraGroups = [ "rtkit" "wheel" "adbusers" "jackaudio" "audio" "networkmanager" "mpd" "libvirtd" "docker" "dialout" "input" ];
		useDefaultShell = false;
		shell = pkgs.zsh;
		openssh.authorizedKeys.keyFiles = (import ../pubKeys).myKeys;
	};

	environment.systemPackages = with pkgs; [ less git git-lfs fd ripgrep wget curl gnutar tmux iperf any-nix-shell direnv vim fzf ix dnsutils usbutils pciutils inetutils lshw kubernetes-helm kubectl exa xsv xmlstarlet rq visidata cachix atop unar ];

	environment.variables = {
		PAGER = "less";
		EDITOR = "vim";
	};

	i18n.defaultLocale = "en_US.UTF-8";

	nix = {
		package = pkgs.nixUnstable;
		extraOptions = ''
			experimental-features = nix-command flakes
		'';
    settings = {
      trusted-users = [ "root" "quinn" ];
      substituters = [
        "https://sigprof.cachix.org"
        "https://veloren-nix.cachix.org"
      ];
      trusted-public-keys = [
        "sigprof.cachix.org-1:oAP3Ahj7n9nhi7N603zyQIAPkqPLd4IbcJd0eUuXJPg="
        "veloren-nix.cachix.org-1:zokfKJqVsNV6kI/oJdLF6TYBdNPYGSb+diMVQPn/5Rc="
      ];
    };
	};

}
