{config, lib, pkgs, ...}:

with lib;

let
	cfg = config.my.emulation;
in {
	imports = [];
	options.my.emulation = {
		overlays = {
			enable = mkEnableOption "Add the emulation overlay to nixpkgs";
			content = mkOption {
				default = [ (import)];
				description = "List of overlays to be applied for emulation packages.";
			};
		};
		udevRules = {
			enable = mkEnableOption "Turn on custom udev rules for controllers etc";
			content = mkOption {
				default = readFile ./gcc.rules;
				type = types.lines;
				description = "See services.udev.extraRules";
			};
		};
		packages = {
			enable = mkEnableOption "Add packages to system";
		};
	};
	config = {
		nixpkgs.overlays = mkIf cfg.overlays.enable cfg.overlays.content;
		services.udev.extraRules = mkIf cfg.udevRules.enable cfg.udevRules.content;
		environment.systemPackages = mkIf cfg.packages.enable [ (pkgs.retroarch.override { cores = with pkgs.libretro; [ desmume dolphin citra beetle-gba ]; }) ];
	};
}
