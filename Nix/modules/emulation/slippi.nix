{ stdenv, fetchFromGitHub, pkgconfig, bluez, ffmpeg, libao, libGLU, libGL, gtk2, gtk3, glib, gettext, xorg, readline, openal, libevdev, portaudio, libusb, libpulseaudio, libudev, gnumake, wget, wxGTK31, soundtouch, miniupnpc, mbedtls, curl, lzo, sfml, enet, xdg_utils, hidapi, cmake}:

stdenv.mkDerivation rec {
	name = "slippi-${version}";
	version = "r18";

	nativeBuildInputs = [ pkgconfig cmake ];
	buildInputs = [ bluez ffmpeg libao libGLU libGL gtk2 gtk3 glib gettext xorg.libpthreadstubs xorg.libXrandr xorg.libXext xorg.libX11 xorg.libSM readline openal libevdev xorg.libXdmcp portaudio libusb libpulseaudio libudev gnumake wget wxGTK31 soundtouch miniupnpc mbedtls curl lzo sfml enet xdg_utils hidapi  ];

	src = fetchFromGitHub {
		owner = "project-slippi";
		repo = "Ishiiruka";
		rev = "1ef6f7bfc12e8566153054e6c964111da44f9125";
		sha256 = "1wyh3rdnn574iawv0cgqk5bjr3zr5f2csh2r81shs77g9635f61y";
	};

	cmakeFlags = [ /*"-DLINUX_LOCAL_DEV=true"*/ "-DENABLE_LTO=true" "-DGTK2_GLIBCONFIG_INCLUDE_DIR=${glib.out}/lib/glib-2.0/include" "-DGTK2_GDKCONFIG_INCLUDE_DIR=${gtk2.out}/lib/gtk-2.0/include" "-DGTK2_INCLUDE_DIRS=${gtk2}/lib/gtk-2.0" ];

}
