{config, lib, pkgs, ...}:

with lib;

let

	cfg = config.my.build;

	builders = {
		quinn-workstation = {
			hostName = "quinn-workstation";
			maxJobs = 1;
			speedFactor = 2;
			supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
			mandatoryFeatures = [ ];
		};
	};

in {
	imports = [];

	options.my.build = {
		enable.quinn-workstation = mkEnableOption "use my workstation as a remote builder";
	};

	config = mkIf (cfg.enable.quinn-workstation) {
		nix.buildMachines = [
			(mkIf cfg.enable.quinn-workstation builders.quinn-workstation)
		];
		nix.extraOptions = ''
			builders-use-substitutes = true
		'';
		nix.distributedBuilds = true;
	};
}
