self: super:

{

	dsp = super.stdenv.mkDerivation {
		pname = "dsp";
		version = "unstable-2020-03-30";
	
		src = super.fetchFromGitHub {
			owner = "bmc0";
			repo = "dsp";
			rev = "9fc7809a5572bf9231cc53493dc4429da8941b8f";
			sha256 = "0km8ab85rn9gqgj1655naq7kq6vlpvn9zhlipz9zw6c0l4rba9ih";
		};
	
		nativeBuildInputs = with super; [
			pkgconfig
		];
	
		buildInputs = with super; [
			fftw
			zita-convolver
			libsndfile
			ffmpeg
			alsaLib
			libao
			libmad
			libpulseaudio
			ladspaH
			libtool
		];
	
	};

}
