{config, lib, pkgs, pkgs-unstable, inputs, ... }:

with lib;

let
	cfg = config.my.graphical;
in {

	options.my.graphical = {
		desktop = {
      latitude = mkOption {
        type = types.str;
        };
      longitude = mkOption {
        type = types.str;
      };
			enable = mkEnableOption "Turn on my desktop settings";
			outputs = mkOption {
				type = types.attrsOf (types.attrsOf types.str);
				description = "Description of video outputs.";
			};
		};
		colors =
		let makeColorOption = name: value:
			mkOption { default = value; type = types.str; description = "A color, in hex."; };
		in lib.attrsets.mapAttrs makeColorOption {
			term0 = "#2c1322";
			term1 = "#422838";
			term2 = "#664a5b";
			term3 = "#806273";
			term4 = "#a7889a";
			term5 = "#c3a2b5";
			term6 = "#dfbdd0";
			term7 = "#ffe2f5";
			term8 = "#b38b19";
			term9 = "#ec4b24";
			term10 = "#e52638";
			term11 = "#db4ba8";
			term12 = "#a855bc";
			term13 = "#0091d1";
			term14 = "#409cb3";
			term15 = "#73ae22";
		};
	};

	config = mkMerge [
		{
		}
		(mkIf cfg.desktop.enable {
    services.xserver = {
      enable = true;
      displayManager.lightdm.enable = true;
      windowManager.openbox.enable = true;
    };
		nixpkgs.overlays = [ (import ./overlay.nix) ];
    services.dbus.enable = true;
		home-manager.users.quinn = { pkgs, ...}: {
      services.gammastep = {
        enable = true;
        latitude = 37.77;
        longitude = -122.43;
      };
			programs.alacritty = {
				enable = true;
				settings = {
					scrolling = { history = 0; };
					dynamic_padding = true;
					font = { size = 11; normal = { family = "IBM Plex Mono"; style = "SemiBold"; }; };
					draw_bold_text_with_bright_colors = true;
					url.launcher.program = "mimeo";
					cursor.style = "Beam";
					colors = {
						primary = { foreground = cfg.colors.term2; background = cfg.colors.term7; };
						normal = {
							black = cfg.colors.term0;
							red = cfg.colors.term1;
							green = cfg.colors.term2;
							yellow = cfg.colors.term3;
							blue = cfg.colors.term4;
							magenta = cfg.colors.term5;
							cyan = cfg.colors.term6;
							white = cfg.colors.term7;
						};
						bright = {
							black = cfg.colors.term8;
							red = cfg.colors.term9;
							green = cfg.colors.term10;
							yellow = cfg.colors.term11;
							blue = cfg.colors.term12;
							magenta = cfg.colors.term13;
							cyan = cfg.colors.term14;
							white = cfg.colors.term15;
						};
						key_bindings = [
							{ key = "V"; mods = "Control|Shift"; action = "Paste"; }
							{ key = "C"; mods = "Control|Shift"; action = "Copy"; }
							{ key = "Add"; mods = "Control"; action = "IncreaseFontSize"; }
							{ key = "Subtract"; mods = "Control"; action = "DecreaseFontSize"; }
						];
					};
				};
			};
      xdg.enable = true;
      xsession = {
        enable = true;
        windowManager.i3 = {
					enable = true;
          extraConfig = "
            exec --no-startup-id greenclip daemon>/dev/null
            ";
					config =
          let
						up = "e";
						down = "n";
						left = "h";
						right = "i";
          in rec {
						assigns = { };
						bars = [ {
							colors = {
								background = cfg.colors.term7;
								separator = "#666666";
								statusline = cfg.colors.term2;
								activeWorkspace = {
									border = "#333333";
									background = "#5f676a";
									text = "#ffffff";
								};
								bindingMode = {
									border = "#2f343a";
									background = "#900000";
									text = "#ffffff";
								};
								inactiveWorkspace = {
									border = cfg.colors.term7;
									background = cfg.colors.term7;
									text = cfg.colors.term2;
								};
								focusedWorkspace = {
									border = cfg.colors.term9;
									background = cfg.colors.term9;
									text = cfg.colors.term7;
								};
								urgentWorkspace = {
									border = "#2f343a";
									background = "#900000";
									text = "#ffffff";
								};
							};
							extraConfig = "";
							fonts = { names = [ "IBM Plex" ]; style = "Mono"; size = 12.0; };
							hiddenState = "hide";
							id = null;
							mode = "dock";
							position = "top";
							trayOutput = "primary";
							workspaceButtons = true;
							workspaceNumbers = true;
						} ];
						colors =
							let
								text = cfg.colors.term2;
							in rec {
							background = cfg.colors.term7;
							focused = rec {
								inherit background text;
								border = cfg.colors.term9;
								childBorder = border;
								indicator = border;
							};
							focusedInactive = rec {
								inherit background text;
								border = cfg.colors.term12;
								childBorder = border;
								indicator = border;
							};
							unfocused = rec {
								inherit background text;
								border = cfg.colors.term12;
								childBorder = border;
								indicator = border;
							};
						};
						floating = {
							border = 2;
							criteria = [ ];
							titlebar = false;
						};
						focus = {
							followMouse = true;
							forceWrapping = true;
							mouseWarping = true;
							newWindow = "smart";
						};
						fonts = { names = [ "IBM Plex" ]; style = "Mono"; size = 12.0; };
						gaps = null;
						keybindings = {
							"F1" = "workspace number 1";
							"F2" = "workspace number 2";
							"F3" = "workspace number 3";
							"F4" = "workspace number 4";
							"F5" = "workspace number 5";
							"F6" = "workspace number 6";
							"F7" = "workspace number 7";
							"F8" = "workspace number 8";
							"F9" = "workspace number 9";
							"F10" = "workspace number 10";
							"F11" = "workspace number 11";
							"F12" = "workspace number 12";
							"Shift+F1" = "move container to workspace number 1";
							"Shift+F2" = "move container to workspace number 2";
							"Shift+F3" = "move container to workspace number 3";
							"Shift+F4" = "move container to workspace number 4";
							"Shift+F5" = "move container to workspace number 5";
							"Shift+F6" = "move container to workspace number 6";
							"Shift+F7" = "move container to workspace number 7";
							"Shift+F8" = "move container to workspace number 8";
							"Shift+F9" = "move container to workspace number 9";
							"Shift+F10" = "move container to workspace number 10";
							"Shift+F11" = "move container to workspace number 11";
							"Shift+F12" = "move container to workspace number 12";
							"${modifier}+Up" = "focus up";
							"${modifier}+Down" = "focus down";
							"${modifier}+Left" = "focus left";
							"${modifier}+Right" = "focus right";
							"${modifier}+Shift+Up" = "move up";
							"${modifier}+Shift+Down" = "move down";
							"${modifier}+Shift+Left" = "move left";
							"${modifier}+Shift+Right" = "move right";
							"${modifier}+Return" = "exec ${pkgs.alacritty}/bin/alacritty";
							"${modifier}+Shift+c" = "reload";
							"${modifier}+Shift+q" = "exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";
							"${modifier}+${left}" = "focus left";
							"${modifier}+${down}" = "focus down";
							"${modifier}+${up}" = "focus up";
							"${modifier}+${right}" = "focus right";
							"${modifier}+Shift+${left}" = "move left";
							"${modifier}+Shift+${down}" = "move down";
							"${modifier}+Shift+${up}" = "move up";
							"${modifier}+Shift+${right}" = "move right";
							"${modifier}+Shift+minus" = "move scratchpad";
							"${modifier}+c" = "kill";
							"${modifier}+Shift+space" = "floating toggle";
							"${modifier}+a" = "focus parent";
							"${modifier}+b" = "splith";
							"${modifier}+d" = menu;
							"${modifier}+u" = "layout toggle split";
							"${modifier}+f" = "fullscreen toggle";
							"${modifier}+minus" = "scratchpad show";
							"${modifier}+r" = "mode resize";
							"${modifier}+s" = "layout stacking";
							"${modifier}+space" = "focus mode_toggle";
							"${modifier}+v" = "splitv";
							"${modifier}+w" = "layout tabbed";
						};
						keycodebindings = { };
						menu = "exec ${pkgs.alacritty}/bin/alacritty -e fmrun";
						modifier = "Mod4";
						startup = [ ];
						terminal = "${pkgs.alacritty}/bin/alacritty";
						window = {
							border = 2;
							commands = [ ];
							hideEdgeBorders = "none";
							titlebar = false;
						};
						workspaceAutoBackAndForth = false;
						workspaceLayout = "default";
					};
        };
      };
			programs.firefox = {
				enable = true;
				extensions = with (import inputs.rycee-nur { inherit pkgs; }).firefox-addons; [
					ublock-origin greasemonkey https-everywhere vimium firefox-color
				];
				profiles =
					let defaultSettings = {
						"app.update.auto" = false;
						"identity.fxaccounts.account.device.name" = config.networking.hostName;
						"signon.rememberSignons" = false;
						"extensions.pocket.enabled" = false;
						"browser.tabs.closeWindowWithLastTab" = false;
						"browser.ctrlTab.recentlyUsedOrder" = false;
						"browser.search.suggest.enabled" = false;
						"general.autoScroll" = true;
						"browser.startup.homepage" = "news.ycombinator.com";
						"browser.newtabpage.enabled" = false;
						"browser.search.hiddenOneOffs" = "Google,Yahoo,Bing,Twitter";
						"network.IDN_show_punycode" = true;
						"network.allow-experiments" = false;
						"services.sync.declinedEngines" = "addons,passwords";
						"services.sync.engine.addons" = false;
						"services.sync.engineStatusChanged.addons" = true;
						"services.sync.engine.passwords" = false;
						"services.sync.engine.prefs" = true;
						"services.sync.engineStatusChanged.prefs" = true;
					};
					in {
						personal = {
							id = 0;
							isDefault = true;
							settings = defaultSettings // {
								"toolkit.legacyUserProfileCustomizations.stylesheets" = true;
							};
							userChrome = builtins.readFile ./firefoxUserChrome.css;
						};
						present = {
							id = 1;
							settings = defaultSettings;
						};
				};
			};
		};
		environment.variables.SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS = "0";
    security.rtkit.enable = true;
		hardware = {
      bluetooth = {
        enable = true;
      };
      xone.enable = true;
			opengl = {
				driSupport = true;
				driSupport32Bit = true;
				extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
			};
			pulseaudio = {
				enable = false;
				systemWide = false;
				support32Bit = true;
				daemon.config.enable-remixing = "no";
				tcp = {
					enable = true;
					anonymousClients.allowedIpRanges = [
						"127.0.0.1"
					];
				};
				zeroconf = {
					discovery.enable = true;
					publish.enable = true;
				};
			};
		};
		programs = {
			dconf.enable = true;
			sway.enable = false;
		};
		networking.networkmanager = {
			enable = true;
		};
    /* ssbm = {
      overlay.enable = true;
      gcc.rules.enable = true;
      gcc.oc-kmod.enable = false;
      cache.enable = true;
    }; */
		environment.systemPackages = with pkgs; [
			pavucontrol mpv pkgs-unstable.youtube-dl imv mimeo libnotify mako flameshot ffmpeg-full dsp cockatrice poppler_utils plover.dev deluge xclip xsel uxn orca-c
		];
    services.udev.extraRules = "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", ATTRS{idProduct}==\"df11\", TAG+=\"uaccess\"";
    programs.adb.enable = true;
    hardware.steam-hardware.enable = true;
	})
	];



}
