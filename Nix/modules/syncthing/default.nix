{
	mySt = {
		devices = {
			quinn-op6t = { name = "quinn-op6t"; id = "JJ5CAQ3-AVQE4TC-CUP5GMS-GMWFHZK-NW2YU76-R5X6R37-EZO2ESB-XKT6CQK"; };
			quinn-op6t2 = { name = "quinn-op6t"; id = "QCLNU3C-M7BFXEJ-XIDEE5U-EHG4P7H-AQIOR6T-FS7JMTL-ZPTJAS7-FL5P4AI"; };
			quinn-op6t3 = { name = "quinn-op6t3"; id = "TQLC3RS-3LMTYS5-RIPBCOF-SKGSM3L-N7OWDPT-MTEVXYW-2GQN5TT-4LF2WAE"; };
			quinn-workstation = { name = "quinn-workstation"; id = "MKYDRSO-VE3SWKP-PSNROXI-KX2NXRK-OVFE64K-LYVOO7X-QXXJNEH-NU3LKQI"; };
			quinn-workstation-2 = { name = "quinn-workstation-2"; id = "OOBJQ7M-DDQFO76-YG5ZC7H-5MIUF5S-3BAMNRQ-FQPCLZC-KICYZRR-4ZYGAQN"; };
		};

		folders = {
			Misc = { id = "yioa73"; };
			MusicSources = { id = "esory177"; };
			MusicPhone = { id = "opussss"; };
		};
	};
}
