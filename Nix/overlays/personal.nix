self: super:

let
	iosevkaPlan = [
		"v-a-doublestorey"
		"v-f-tailed"
		"v-i-italic"
		"v-l-italic"
		"v-zero-14"
		"v-tilde-low"
		"v-asterisk-hexlow"
		"v-underscore-high"
		"v-paragraph-low"
		"v-g-opendoublestorey"
		"v-m-normal"
		"v-caret-low"
		"v-at-short"
		"v-eszet-sulzbacher"
		"v-t-cross"
		"v-capital-q-taily"
		"v-numbersign-slanted"
		"v-three-twoarcs"
		"v-y-cursive"
		"v-one-base"
		"v-dollar-throughcap"
		"v-percent-rings"
		"v-lig-ltgteq-flat"
		"v-m-shortleg"
		"ligset-haskell" ];
#	unstable = (import <nixpkgs-unstable> {}).pkgs;
in
{
	iosevka-term = super.iosevka.override rec {
		set = "term";
		privateBuildPlan = {
			family = "Iosevka Term";
			design = iosevkaPlan ++ [ "sp-fixed" ];
		};
	};
	iosevka-term-lig = super.iosevka.override rec {
		set = "term-lig";
			privateBuildPlan = {
				family = "Iosevka TermLig";
				design = iosevkaPlan ++ [ "sp-term" ];
			};
	};
	iosevka-type = super.iosevka.override rec {
		set = "type";
			privateBuildPlan = {
				family = "Iosevka Type";
				design = iosevkaPlan;
			};
	};
	my-tex = super.texlive.combine {inherit (super.texlive) scheme-context scheme-basic collection-luatex collection-metapost; };
	full-tex = super.texlive.combine {inherit (super.texlive) scheme-full; };
	myWeechat = super.weechat.override {configure = {availablePlugins, ...}: {
		scripts = with self.weechatScripts; [ weechat-autosort ];
    plugins = with availablePlugins; [ python perl ];
	};};
}
