nix-shell -p nixUnstable --run "nix build .#nixosConfigurations.$1.config.system.build.toplevel --experimental-features flakes\ nix-command"
nixos-install --system ./result
